# SimpleApp By - Chinmay Pimpalgaonkar
# Documentation to Set up and Run CICD Pipeline

## Introduction
CI/CD helps automate the build, test, and deployment processes, leading to faster development cycles and improved software quality. Basically there is auto updation in the deployed application. When the new change comes in the application, by doing a single task, whole application get modified and deployed by single commit in the project so no need to do redundant tasks.

## Prerequisites
- Operating System (OS) : You must have OS (e.g., windows, macOs, ubuntu etc) on which you will work on your project in your machine.
- Version Control System (VCS) : In your machine, version control system like Github, Gitlab or Bitbucket must be set up to host the source code.
- CI/CD Tools : You'll need CI/CD tools such as Jenkins, GitLab CI/CD, or GitHub Actions set up and configured.
- Deployment Target: Decide on the target environment for deploying your application, whether it's a staging server, production server, or a cloud platform like AWS, Azure, or GCP or as per the assignemnt it can be localhost.

- Access Credentials: Obtain necessary access credentials for your deployment target (e.g., SSH keys, API tokens, AWS IAM credentials).

## Installation Steps:

- Install and Configure CI/CD Tool: Install and configure your chosen CI/CD tool (e.g., Jenkins, GitLab CI/CD) on a server or cloud instance.

- Install Required Dependencies: Install any dependencies required by your CI/CD pipeline, such as Node.js, npm, Docker, etc

Note : Above things can be installed from any browser on windows


## Configuration Details:
- Project Setup: Set up your project repository on your Version Control System and push your code to it.
 
- Configure CI/CD Pipeline: Define your CI/CD pipeline using a configuration file (e.g., .gitlab-ci.yml, Jenkinsfile) in the root of your project repository.
Define stages, jobs, and steps for building, testing, and deploying your application.

#### Code that I have used ( Jenkinsfile ) :

``` 
pipeline {
    agent any
    
    stages {
        stage('Checkout') {
            steps {
                git branch: 'main', credentialsId: '401d9015-f33a-4552-9204-df75f6bda180', url: 'https://gitlab.com/chinmay410909/simpleapp.git'
            }
        }
        
        stage('Build') {
            steps {
                // building node js application 
                bat 'npm install'
                bat 'npm init @eslint/config'
                bat 'npm install -g eslint'
                bat 'npm install -g jest'
            }
        }
        
        stage('Test') {
            steps {
                // executing basic test for application 
                // syntax check and linting
                bat 'npm run lint'
                // Unit testing 
                bat 'npm test -- --passWithNoTests'
                bat 'npm run'

                bat 'npm run test:unit'
                // Integration Testing
                bat 'npm run test:integration'
                
            }
        }
        stage('Deploy') {
            steps {
                // deployin application on local host
                bat 'node index.js /dev/null 2>&1 &'
            }
        }
    }
}
```

In the above code, I added important commands to build, test and deploy node js application. But to Checkout the project, I have used step as :

``` 
steps {
        git branch: 'main', credentialsId: '401d9015-f33a-4552-9204-df75f6bda180', url: 'https://gitlab.com/chinmay410909/simpleapp.git'
}

```

which is unique for me. To configure it for yourself, go to the Configuration of your Jenkins Intent and create a pipeline or using SCM import Jenkinsfile from the gitlab.


- Secrets Management: Manage sensitive information such as access tokens, API keys, and passwords securely using built-in features of your CI/CD tool or external secret management solutions.

- Integration with Deployment Target: Configure your CI/CD pipeline to deploy your application to the target environment. This may involve setting up SSH, API, or other access methods.

- Webhook Configuration: Set up webhooks or triggers in your Version Control System to notify your CI/CD tool of new commits or changes in the repository.

![File to the host](screenshots/webhook.png)

- You have to add trigger jenkins stage for the webhook configuration and CICD :
My gitlab-ci.yml file is :
```
image: node

stages:
  - build
  - test
  - deploy
  - trigger_jenkins

build:
  stage: build
  script:
    - npm install
    - npm install -g eslint
  artifacts:
    paths:
      - node_modules
      - package-lock.json

test:
  stage: test
  script:
    - echo "Testing"
    # - npm run lint
    # - npm test -- --passWithNoTests

deploy_to_test:
  stage: deploy
  script:
    - echo "Deploying to test environment"
    - node index.js /dev/null 2>&1 &
  environment:
    name: test
    url: http://localhost:3000/ # URL of your test environment

trigger_jenkins:
  stage: trigger_jenkins
  script:
    - curl -X POST "https://3bca-2402-8100-3172-c45c-403c-bd55-b2d6-abf6.ngrok-free.app/webhook"
```
In the above code 
``` 
trigger_jenkins:
  stage: trigger_jenkins
  script:
    - curl -X POST "https://3bca-2402-8100-3172-c45c-403c-bd55-b2d6-abf6.ngrok-free.app/webhook"
```
In script, after POST url will be different for you.

- Monitoring and Notifications: Set up monitoring and notification systems to track the progress of your CI/CD pipeline and receive alerts in case of failures or issues.


## Running the CI/CD Pipeline:

- Commit and Push Changes: Make changes to your project code and commit them to the repository. Push the changes to your Version Control System.

![File to the host](screenshots/pipeline.png)


- Automatic Triggering: Your CI/CD pipeline should be automatically triggered by the webhook or trigger configured in the Version Control System. Basically, It should execute job in the jenkins project/intent.

- Monitor Pipeline Execution: Monitor the execution of your CI/CD pipeline in the CI/CD tool's dashboard or interface. Check the logs and output of each stage and job to ensure everything is running smoothly.

![File to the host](screenshots/stageView.png)

- Troubleshooting: In case of failures or issues, we can troubleshoot the problem by examining the logs, identifying the root cause, and making necessary adjustments to your pipeline configuration.

- Deployment and Verification: Once the pipeline completes successfully, verify that your application is deployed and functioning as expected in the target environment. Here we deployed on the localhost.

![File to the host](screenshots/serverDeployment.png)

For our case application will be deployed on http://localhost:3000 by the jenkins

![File to the host](screenshots/output.png)

***



